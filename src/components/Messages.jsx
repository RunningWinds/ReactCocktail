export const Messages = () => {
  return (
    <div className="row">
      <div className="col-lg-4">
        <div className="row mb-2">
          <div className="col-3">
            <img src={require('./img/r3.jpg')} alt=""/>
          </div>
          <div className="col-9">
            <h5>Марина</h5>
            <p>А поехали на рыбалку!</p>
          </div>
        </div>
        <div className="row mb-2">
          <div className="col-3">
            <img src={require('./img/r9.jpg')} alt=""/>
          </div>
          <div className="col-9">
            <h5>Семен Семеныч</h5>
            <p>Да, смешной анекдот )</p>
          </div>
        </div>
        <div className="row mb-2">
          <div className="col-3">
            <img src={require('./img/r12.jpg')} alt=""/>
          </div>
          <div className="col-9">
            <h5>Ибрагим</h5>
            <p>Харашо, брат</p>
          </div>
        </div>
        <div className="row mb-2">
          <div className="col-3">
            <img src={require('./img/r10.jpg')} alt=""/>
          </div>
          <div className="col-9">
            <h5>Кристина</h5>
            <p>Добрый день!</p>
          </div>
        </div>
      </div>
      <div className="col-lg-8">
        <div className="row mb-3">
          <div className="col-3">
            <img src={require('./img/r2.jpg')} alt=""/>
          </div>
          <div className="col-9">
            <h5>Сидор Петрович</h5>
            <p>Да, спасибо. Заеду завтра!</p>
          </div>
        </div>
        <div className="row mb-5">
          <div className="col-3">
            <img src={require('./img/r7.jpg')} alt=""/>
          </div>
          <div className="col-9">
            <h5>Иван Иванов</h5>
            <p>Отлично!</p>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <textarea className="form-control" rows="5" placeholder="Текст сообщения"></textarea>
          </div>
          <div className="col-6 offset-6 mt-3">
            <button className="btn btn-primary form-control">Отправить</button>
          </div>
        </div>
      </div>
    </div>
  );
};
