export const Profile = (props) => {
  let userId = location.pathname.split("/")[2];
  let user = props.function(userId);
  return (
    <div className="row">
      <div className="col-md-3">
        <img className="img-fluid" src={user.avatar} alt="photo" width="100%" />
      </div>
      <div className="col-md-9">
        <h1>
          Id: <span id="index">{user.id}</span>
        </h1>
        <h2>
          Фамилия и имя: <span id="userName">{user.name} {user.lastname}</span>
        </h2>
        <h3>
          Email: <span id="email">{user.email}</span>
        </h3>
        <h3>О себе</h3>
        <p>
          {user.about}
        </p>
      </div>
    </div>
  );
};
