const user = {
	name: "Иван",
	lastname: "Иванов",
	email: "ivanov@ya.ru",
	id: 1,
	about:
		"Учитывая ключевые сценарии поведения, перспективное планирование напрямую зависит от поставленных обществом задач! Но предприниматели в сети интернет и по сей день остаются уделом либералов, которые жаждут быть описаны максимально подробно.",
	avatar:
		"https://images.unsplash.com/photo-1633332755192-727a05c4013d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8dXNlcnxlbnwwfHwwfHw%3D&w=200&q=80",
};

//const users = {
 // 0: { name: "Никита", lastname: "Сидоренко", id: 4, avatar: "https://source.unsplash.com/random/?user&w=200", },
//  1: { name: "Станислав", lastname: "Потапов", id: 6, avatar: "https://source.unsplash.com/random/?user&w=200", },
//  2: { name: "Даниил", lastname: "Кузнецов", id: 8, avatar: "https://source.unsplash.com/random/?user&w=200", },
//  3: { name: "Александр", lastname: "Красновский", id: 12, avatar: "https://source.unsplash.com/random/?user&w=200", }
//};

let users = [];
export function getUser(userId) {
	for (let i = 0; i< users.length; i++) {
		if (users[i].id == userId) return users[i];
	}
  return user;
}
export async function getUsers() {
	let response = await fetch("https://0360.napli.ru/getUsers");
		users = await response.json();
	
		return users;
	}